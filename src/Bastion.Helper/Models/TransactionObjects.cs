﻿namespace Bastion.Helper
{
    using System.Collections.Generic;

    public class TransactionObjects
    {
        public List<object> ObjectsToAdd { get; set; } = new List<object>();
        public List<object> ObjectsToUpdate { get; set; } = new List<object>();
        public List<object> ObjectsToDelete { get; set; } = new List<object>();
    }
}

﻿namespace Bastion.Helper.Interfaces
{
    using Newtonsoft.Json;

    /// <summary>
    /// Represents an interface than can be notified via hub.
    /// </summary>
    public interface INotifiable
    {
        /// <summary>
        /// Gets SignalR method.
        /// </summary>
        [JsonIgnore]
        string NotificationMethod { get; }
    }
}

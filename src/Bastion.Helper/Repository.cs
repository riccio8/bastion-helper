﻿namespace Bastion.Helper
{
    using Bastion.Helper;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Linq.Expressions;

    public class Repository<TContext> where TContext: DbContext, new()
    {
        protected readonly TContext context;

        public Repository(TContext context)
        {
            this.context = context;
        }

        public Repository()
        {
            this.context = new TContext();
        }

        public virtual async Task<IList<T>> GetEntities<T>(Expression<Func<T, bool>> predicate = null, params string[] includes)
            where T : class
        {
                IQueryable<T> set = this.context.Set<T>();
                if (predicate != null)
                {
                    set = set.Where(predicate);
                }

                foreach (string include in includes)
                {
                    set = set.Include(include);
                }

                return await set.ToListAsync();
        }

        public virtual async Task<IList<T>> GetLastEntities<T>(Expression<Func<T, bool>> predicate = null, int lastCount = 1)
            where T : class
        {
                IQueryable<T> set = this.context.Set<T>();
                if (predicate != null)
                {
                    set = set.Where(predicate);
                }

                var totalCount = set.Count();
                return await set.Skip(Math.Max(totalCount - lastCount, 0)).ToListAsync();
        }

        public virtual async Task<T> GetEntity<T>(Expression<Func<T, bool>> predicate, params string[] includes) where T : class
        {
            IQueryable<T> set = this.context.Set<T>();
            foreach (string include in includes)
            {
                set = set.Include(include);
            }

            return await set.FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<T> GetFirstEntityAsync<T>(Expression<Func<T, bool>> predicate = null) where T : class
        {
                return await (predicate != null ? this.context.Set<T>().FirstOrDefaultAsync(predicate) : this.context.Set<T>().FirstOrDefaultAsync());
        }

        public virtual async Task<T> GetLastEntityAsync<T>(Expression<Func<T, bool>> predicate = null) where T : class
        {
            return await (predicate != null ? this.context.Set<T>().LastOrDefaultAsync(predicate) : this.context.Set<T>().LastOrDefaultAsync());
        }

        public virtual async Task<int> GetCountEntityAsync<T>(Expression<Func<T, bool>> predicate = null) where T : class
        {
            IQueryable<T> set = this.context.Set<T>();
            if (predicate != null)
            {
                set = set.Where(predicate);
            }
            return await set.CountAsync();
        }

        public virtual async Task<TResult> GetMinAsync<TSource, TResult>(Expression<Func<TSource, TResult>> selector) where TSource : class
        {
                return await this.context.Set<TSource>().MinAsync(selector);
        }

        public virtual async Task<TResult> GetMaxAsync<TSource, TResult>(Expression<Func<TSource, TResult>> selector) where TSource : class
        {
            return await this.context.Set<TSource>().MaxAsync(selector);
        }

        public virtual async Task<IEnumerable<T>> AddEntities<T>(IEnumerable<T> objects) where T : class
        {
            this.context.AddRange(objects);
            await this.context.SaveChangesAsync();

            return objects;
        }

        public virtual async Task<T> AddEntity<T>(T obj) where T : class
        {
            this.context.Add(obj);
            await this.context.SaveChangesAsync();

            return obj;
        }


        public virtual async Task<IEnumerable<T>> UpdateEntities<T>(IEnumerable<T> objects) where T : class
        {
            this.context.UpdateRange(objects);
            await this.context.SaveChangesAsync();

            return objects;
        }

        public virtual async Task UpdateEntity(object obj)
        {
            this.context.Update(obj);
            await this.context.SaveChangesAsync();
        }

        public virtual async Task DeleteEntities(IEnumerable<object> objects)
        {
            this.context.RemoveRange(objects);
            await this.context.SaveChangesAsync();
        }

        public virtual async Task DeleteEntity(object obj)
        {
            this.context.Remove(obj);
            await this.context.SaveChangesAsync();
        }

        public virtual async Task<int> TotalCount<T>(Expression<Func<T, bool>> predicate = null) where T : class
        {
                if (predicate == null)
                {
                    return await this.context.Set<T>().CountAsync();
                }
                else
                {
                    return await this.context.Set<T>().Where(predicate).CountAsync();
                }
        }

        public virtual async Task<IList<T>> GetEntitiesPage<T, TKey>(int page, int perPage,
            Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, TKey>> orderByFunc = null,
            Expression<Func<T, TKey>> orderByDescendingFunc = null,
            params string[] includes) where T : class
        {
                IQueryable<T> set = this.context.Set<T>();
                if (predicate != null)
                {
                    set = set.Where(predicate);
                }

                if (orderByFunc != null)
                {
                    set = set.OrderBy(orderByFunc);
                }

                if (orderByDescendingFunc != null)
                {
                    set = set.OrderByDescending(orderByDescendingFunc);
                }

                foreach (string include in includes)
                {
                    set = set.Include(include);
                }

                set = set.Skip((page - 1) * perPage).Take(perPage);
                return await set.ToListAsync();
        }

        public virtual async Task<int> GetTotalPage<T>(int perPage, Expression<Func<T, bool>> predicate = null) where T : class
        {
                int total = await TotalCount<T>(predicate);
                int totalPage = total / perPage;
                if (total % perPage > 0)
                {
                    totalPage++;
                }

                return totalPage;
        }

        public virtual async Task<decimal> GetSum<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, decimal>> sumFunc)
            where T : class
        {
                return await this.context.Set<T>().Where(predicate).SumAsync(sumFunc);
        }

        public virtual async Task EntitiesTransaction(IEnumerable<object> objectsToAdd, IEnumerable<object> objectsToUpdate,
            IEnumerable<object> objectsToDelete)
        {
            if (objectsToAdd != null) this.context.AddRange(objectsToAdd);
            if (objectsToUpdate != null) this.context.UpdateRange(objectsToUpdate);
            if (objectsToDelete != null) this.context.RemoveRange(objectsToDelete);
            var count = await context.SaveChangesAsync();
        }

        public virtual async Task EntitiesTransaction(TransactionObjects transactionObjects)
        {
            await this.EntitiesTransaction(transactionObjects.ObjectsToAdd, transactionObjects.ObjectsToUpdate, transactionObjects.ObjectsToDelete);
        }

        public virtual async Task<T> GetFirstEntity<T>() where T : class
        {
            return await this.context.Set<T>().FirstOrDefaultAsync();
        }
    }

}
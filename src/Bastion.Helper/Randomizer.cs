﻿namespace Bastion.Helper
{
    using System.Collections.Generic;
    using System;

    public static class Randomizer
    {
        private static readonly Random random = new Random();

        public static string GenerateRandomString(int length = 5)
        {
            if (length < 2)
            {
                length = 2;
            }

            const string vowel = "aeuy";
            const string consonants = "bcdfghjklmnpqrstvwx";
            const string digits = "0123456789";

            char[] buffer = new char[length];
            int i = 0;
            for (; i < length - 2; ++i)
            {
                if (random.Next(4) % 3 == 0)
                {
                    buffer[i] = consonants[random.Next(consonants.Length)];
                    if (random.Next(4) == 0)
                    {
                        buffer[i] = char.ToUpper(buffer[i]);
                    }
                }
                else
                {
                    buffer[i] = vowel[random.Next(vowel.Length)];
                    if (random.Next(4) == 0)
                    {
                        buffer[i] = char.ToUpper(buffer[i]);
                    }
                }
            }

            for (; i < length; ++i)
            {
                buffer[i] = digits[random.Next(digits.Length)];
            }

            return new string(buffer);
        }

        public static int GetRandomIndex(int maxIndex)
        {
            return random.Next(0, maxIndex);
        }

        public static T GetRandomElement<T>(this IList<T> collection)
        {
            return collection[GetRandomIndex(collection.Count)];
        }

        public static decimal GetRandomPercent(double minPercent, double maxPercent)
        {
            decimal percent = random.Next( (int)(minPercent * 100),(int)(maxPercent * 100)) / 100m;
            percent = decimal.Round(percent, 2);
            return percent;
        }

        public static decimal GetRandomPrice(int minPrice, int maxPrice)
        {
            decimal percent = random.Next(minPrice * 100, maxPrice * 100) / 100m;
            percent = decimal.Round(percent, 2);
            return percent;
        }

        public static decimal GetRandomPrice(decimal price, double deviationPercent)
        {
            decimal randomPercent = GetRandomPercent(-deviationPercent, deviationPercent);
            decimal newPercentValue = 100 + randomPercent;
            return decimal.Round(price * newPercentValue / 100, 2);
        }

        public static int GetRandomInt(int min, int max)
        {
            int percent = random.Next(min, max + 1);
            return percent;
        }

        public static bool GetRandomBool()
        {
            return random.Next(0, 2) == 0;
        }
    }
}

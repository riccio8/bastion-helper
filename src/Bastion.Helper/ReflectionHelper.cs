﻿namespace Bastion.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Support functional via reflection
    /// </summary>
    public static class ReflectionHelper
    {
        public static void CopyFields(object sourceObject, object targetObject)
        {
            var sourceProperties = sourceObject.GetType().GetProperties();
            var targetProperties = targetObject.GetType().GetProperties();

            foreach (PropertyInfo sourceProperty in sourceProperties)
            {
                var targetProperty = targetProperties.FirstOrDefault(x => x.Name == sourceProperty.Name);
                if (targetProperty != null && targetProperty.PropertyType.Equals(sourceProperty.PropertyType))
                {
                    var value = sourceProperty.GetValue(sourceObject);
                    targetProperty.SetValue(targetObject, value);
                }
            }
        }

        public static void CopyNotNullFields(object sourceObject, object targetObject)
        {
            var sourceProperties = sourceObject.GetType().GetProperties();
            var targetProperties = targetObject.GetType().GetProperties();

            foreach (PropertyInfo sourceProperty in sourceProperties)
            {
                var targetProperty = targetProperties.FirstOrDefault(x => x.Name == sourceProperty.Name);
                if (targetProperty != null && targetProperty.PropertyType.Equals(sourceProperty.PropertyType))
                {
                    var value = sourceProperty.GetValue(sourceObject);
                    if (value != null)
                    {
                        targetProperty.SetValue(targetObject, value);
                    }
                }
            }
        }

        /// <summary>
        /// Update objects.
        /// </summary>
        /// <typeparam name="TSource">Type source.</typeparam>
        /// <typeparam name="TTarget">Type target.</typeparam>
        /// <param name="sourceObjects">Source objects.</param>
        /// <param name="targetObjects">Target objects.</param>
        /// <param name="transactionObjects">Transaction objects.</param>
        /// <param name="compareAction">Compare action.</param>
        /// <param name="updateAction">Update action.</param>
        public static void UpdateObjects<TSource, TTarget>(
            ref TransactionObjects transactionObjects,
            IEnumerable<TSource> sourceObjects,
            IEnumerable<TTarget> targetObjects,
            Func<TSource, TTarget, bool> compareAction,
            Action<TSource, TTarget> updateAction = null)
        {
            if (sourceObjects == null)
            {
                transactionObjects.ObjectsToDelete.AddRange(targetObjects.Cast<object>());
                return;
            }

            foreach (var targetObject in targetObjects)
            {
                var sourceObject = sourceObjects.FirstOrDefault(x => compareAction(x, targetObject));
                if (sourceObject == null)
                {
                    transactionObjects.ObjectsToDelete.Add(targetObject);
                }
                else
                {
                    CopyNotNullFields(sourceObject, targetObject);
                    updateAction?.Invoke(sourceObject, targetObject);
                    transactionObjects.ObjectsToUpdate.Add(targetObject);
                }
            }

            foreach (var sourceObject in sourceObjects)
            {
                var targetObject = targetObjects.FirstOrDefault(x => compareAction(sourceObject, x));
                if (targetObject == null)
                {
                    targetObject = Activator.CreateInstance<TTarget>();
                    CopyNotNullFields(sourceObject, targetObject);
                    updateAction?.Invoke(sourceObject, targetObject);
                    transactionObjects.ObjectsToAdd.Add(targetObject);
                }
            }
        }

        /// <summary>
        /// Compare delegate.
        /// </summary>
        /// <typeparam name="TSource">Type source.</typeparam>
        /// <typeparam name="TTarget">Type target.</typeparam>
        /// <param name="sourceObject">Source object.</param>
        /// <param name="targetObject">Target object.</param>
        public delegate bool CompareDelegate<TSource, TTarget>(TSource sourceObject, TTarget targetObject);

    }
}

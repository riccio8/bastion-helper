﻿namespace Bastion.Helper
{
    using Bastion.Helper.Exceptions;

    public static class CustomValidator
    {
        private const int notFoundCode = 404;
        private const int badRequestCode = 404;


        public static void CheckStringValue(string checkString, string name)
        {
            if (string.IsNullOrWhiteSpace(checkString))
            {
                throw new CustomException($"Pamameter {name} is null or empty.", notFoundCode);
            }
        }

        public static void CheckPositive(decimal CheckValue, string name)
        {
            if (CheckValue <= 0)
            {
                throw new CustomException($"Pamameter {name} is null or not positive.", badRequestCode);
            }
        }

        public static void CheckNotNull(object CheckValue, string msg)
        {
            if (CheckValue == null)
            {
                throw new CustomException(msg, notFoundCode);
            }
        }

        public static void Check(bool checkValue, string msg)
        {
            if (!checkValue)
            {
                throw new CustomException(msg, badRequestCode);
            }
        }

        public static void CheckNull(object CheckValue, string msg)
        {
            if (CheckValue != null)
            {
                throw new CustomException(msg, badRequestCode);
            }
        }
    }
}

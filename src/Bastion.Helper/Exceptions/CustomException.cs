﻿namespace Bastion.Helper.Exceptions
{
    using System;
    public class CustomException: Exception
    {
        public CustomException(string message, int code = 0) : base(message)
        {
            HResult = code;
        }
    }
}

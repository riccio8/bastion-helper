﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Bastion.Helper.Test.Models
{
    public class ServerTime
    {
        [JsonProperty(PropertyName = "serverTime")]
        public long SrvTime { get; set; }
    }
}
